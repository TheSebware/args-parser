package tk.andr3as07.util.args;

class ArgsDemo {

	public static void main(final String[] args) {
		final ArgsParser parser = new ArgsParser();
		parser.addArgument("path", true, 'p');
		parser.addArgument("port", true);

		parser.addArgument("test", "Das ist ein Test", 't');
		parser.addArgument("test2", true);

		final String arguments = "-p C:\\Program\\ Files\\ (x86)\\FileZilla\\ FTP\\ Client --port 25157 --test2 This\\ is\\ the\\ argument\\ which\\ contains\\ 2\\ \\\\\\ isn't\\ that\\ crazy soTotallyNotASpaceIPromise";

		final ArgsResult result = parser.parse(arguments);
		System.out.println(result.getString("test"));
		System.out.println(result.getString("test2"));
		System.out.println(result.getString("path"));
		System.out.println(result.getInteger("port") - result.getInteger("port"));
	}
}